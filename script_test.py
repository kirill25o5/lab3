from sklearn.metrics import accuracy_score

if __name__ == '__main__':

    with open('predict.txt', 'r') as f:
        pred_y = f.read().split('\n')
    with open('true.txt', 'r') as f:
        test_y = f.read().split('\n')

    accuracy = accuracy_score(test_y, pred_y)
    threshold = 0.9
    # Проверяем, прошел ли тест
    if accuracy >= threshold:
        exit()
    else:
        exit(-1)
