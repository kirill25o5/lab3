from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import load_iris
from sklearn.metrics import accuracy_score
import numpy as np

data = load_iris()
X = data.data
y = data.target

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, random_state=42)

clf = RandomForestClassifier(n_estimators=300, random_state=42, max_depth=24, min_samples_split=3, min_samples_leaf=4)
clf.fit(X_train, y_train)

y_pred = clf.predict(X_test)

np.savetxt('predict.txt', y_pred, fmt='%d', delimiter='\n')
np.savetxt('true.txt', y_test, fmt='%d', delimiter='\n')

